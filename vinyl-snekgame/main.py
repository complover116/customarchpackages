#!/bin/python
import random
import sys
from enum import Enum
from PySide6.QtWidgets import QApplication, QMainWindow, QFrame
from PySide6.QtGui import QPainter, QColor
from PySide6.QtCore import Qt, QBasicTimer


class Window(QMainWindow):
    def __init__(self):
        super(Window, self).__init__(parent=None)
        self.setCentralWidget(SnekGame(self))
        self.setFixedSize(800, 800)
        self.setWindowTitle('Snek game')
        self.show()


class Direction(Enum):
    RIGHT = 1
    LEFT = 2
    UP = 3
    DOWN = 4


class SnekGame(QFrame):
    def __init__(self, parent):
        super(SnekGame, self).__init__(parent)
        self.setWindowTitle("Snek Game")
        self.setFocusPolicy(Qt.FocusPolicy.StrongFocus)

        self.x_dimension = 50
        self.y_dimension = 50

        self.starting_position = [self.x_dimension // 2, self.y_dimension // 2]
        # list containing all the snek segments positions
        self.snek_segments = [self.starting_position]
        for i in range(1, self.starting_position[0] // 2):
            self.snek_segments.append([self.starting_position[0] + i, self.starting_position[1]])
        self.snek_segments.reverse()

        self.apple_coordinates = [0, 0]

        # enum variable that contains snek direction - either RIGHT, LEFT, UP or DOWN
        self.snek_direction = Direction.RIGHT

        # timer, that will update the game after a certain amount of time
        self.timer = QBasicTimer()
        self.speed = 200

        # game start
        self.timer.start(200, self)
        self.generate_apple()

    def paintEvent(self, event):
        p = QPainter(self)
        rect = self.contentsRect()
        top = rect.bottom() - self.y_dimension * self.square_height()

        # drawing the snek
        for segment in self.snek_segments:
            self.draw_square(p, QColor(Qt.GlobalColor.green), rect.left() + segment[0]*self.square_width(),
                             top + segment[1]*self.square_height())

        # drawing the apple
        self.draw_square(p, QColor(Qt.GlobalColor.red), rect.left() + self.apple_coordinates[0]*self.square_width(),
                         top + self.apple_coordinates[1]*self.square_height())

    def draw_square(self, painter, color, x, y):
        painter.fillRect(x, y, self.square_width(), self.square_height(), color)

    def square_width(self):
        return self.contentsRect().width() / self.x_dimension

    def square_height(self):
        return self.contentsRect().height() / self.y_dimension

    def generate_apple(self):
        # making sure that the new apple won't be on top of the snek
        coordinates = self.snek_segments[0]
        while coordinates in self.snek_segments:
            coordinates = [random.randint(0, self.x_dimension - 1), random.randint(0, self.y_dimension - 1)]
        self.apple_coordinates = coordinates

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key.Key_D and self.snek_direction != Direction.LEFT:
            self.snek_direction = Direction.RIGHT
        if key == Qt.Key.Key_A and self.snek_direction != Direction.RIGHT:
            self.snek_direction = Direction.LEFT
        if key == Qt.Key.Key_W and self.snek_direction != Direction.DOWN:
            self.snek_direction = Direction.UP
        if key == Qt.Key.Key_S and self.snek_direction != Direction.UP:
            self.snek_direction = Direction.DOWN

    def timerEvent(self, event):
        direction = self.snek_direction
        apple = self.apple_coordinates
        head = [self.snek_segments[0][0], self.snek_segments[0][1]]

        # 1) Add a snek segment in the direction that the snek is looking
        if direction == Direction.RIGHT:
            self.snek_segments.insert(0, [head[0] + 1, head[1]])
        if direction == Direction.LEFT:
            self.snek_segments.insert(0, [head[0] - 1, head[1]])

        if direction == Direction.UP:
            self.snek_segments.insert(0, [head[0], head[1] - 1])
        if direction == Direction.DOWN:
            self.snek_segments.insert(0, [head[0], head[1] + 1])

        # 2) If there are any collisions, end the game
        new_head = [self.snek_segments[0][0], self.snek_segments[0][1]]
        old_snek_segments = []
        old_snek_segments.extend(self.snek_segments)
        old_snek_segments.remove(new_head)

        if new_head[0] >= self.x_dimension or new_head[0] < 0 or new_head[1] >= self.y_dimension or new_head[1] < 0 \
                or new_head in old_snek_segments:
            self.timer.stop()
            self.update()
            return

        # 3) If the sneks head is inside the apple sector, generate a new apple, else remove the last segment of
        # the snek
        if new_head[0] == apple[0] and new_head[1] == apple[1]:
            self.generate_apple()
        else:
            del self.snek_segments[-1]

        # 4) The snek gets faster the longer it grows
        if len(self.snek_segments) > 5:
            self.timer.start(150, self)
        if len(self.snek_segments) > 10:
            self.timer.start(125, self)
        if len(self.snek_segments) > 15:
            self.timer.start(75, self)
        if len(self.snek_segments) > 20:
            self.timer.start(50, self)

        # 5) Redraw the frame
        self.update()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window()
    sys.exit(app.exec())
